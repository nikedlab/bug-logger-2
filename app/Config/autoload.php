<?php
\App::import('Vendor', 'Symfony/Component/ClassLoader/UniversalClassLoader');

$loader = new \Symfony\Component\ClassLoader\UniversalClassLoader();

$loader->registerNamespaces(array(
    'app' => dirname(APP),
    'Symfony' => VENDORS,
    'CakePhpDependencyInjection' => realpath(CakePlugin::path('CakePhpDependencyInjection') . '../')
));

$loader->registerPrefixes(array(
    'Twig_' => VENDORS . 'Twig' . DS . 'lib',
));

$loader->register();

<?php
/**
 * Created by PhpStorm.
 * User: nike
 * Date: 9/9/13
 * Time: 12:10 AM
 */

class UsersController extends AppController {

    public function login() {
        if ($this->Auth->login()) {
            $this->redirect($this->Auth->redirectUrl());
        } else {
            $this->Session->setFlash('Not able to login');
        }
    }

    public function logout() {
        $this->redirect($this->Auth->logout());
    }

}

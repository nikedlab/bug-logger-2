<?php
App::uses('Controller', 'Controller');

class AppController extends Controller {
    public $viewClass = 'CakePhpTwig.Twig';
    public $components = array('DebugKit.Toolbar',
        'Auth' => array(
            'authenticate' => array('Basic')
        ),
        "Session"
    );
    public $helpers = array("jGrowl", "Html");

    public function beforeFilter() {
        parent::beforeFilter();

        $this->Auth->loginAction = array('controller' => 'users', 'action' => 'login');
        $this->Auth->logoutRedirect = array('controller' => 'users', 'action' => 'login');
        $this->Auth->loginRedirect = array('controller' => 'pages', 'action' => 'index');
    }

    function _setFlash($msg, $type = 'message', $key = 'flash') {
        $types = array('error', 'warning', 'message', 'success');

        if(!in_array($type, $types)) {
            $type = 'message';
        }

        if(empty($key)) {
            $key = 'flash';
        }

        $flash = array(
            'type' => $type,
            'message' => __($msg, true)
        );

        if($this->Session->check('FlashMessage.' . $key)) {
            $flashData = $this->Session->read('FlashMessage.' . $key);

            array_push($flashData, $flash);
        } else {
            $flashData[] = $flash;
        }

        $this->Session->write('FlashMessage.' . $key, $flashData);
    }


}

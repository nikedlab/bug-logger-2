<?php

App::uses('AppController', 'Controller');

class LogsController extends AppController {

    public $uses = array('Log');

    public function beforeFilter() {
        parent::beforeFilter();
        $this->Auth->allow('writeLog');
    }


    public function index() {

        $appList = $this->Log->find("all",
            array(
                "fields" => array(
                    "package_name"
                ),
                "group" => "package_name"
            )
        );
        $rows = Set::extract("/Log/.", $appList);
        $this->set("appList", $rows);
        $this->set("title_for_layout", "Bug tracker 2.0");
    }

    public function writeLog() {
        if($this->request->is("post")) {
            $data["Log"] = array();

            foreach ($this->request->data as $k => $v) {
                $data[mb_strtolower($k)] = $v;
            }

            $this->Log->save($data);
        }
    }

    public function showLogs($id) {
        if(empty($id)) {
            $this->redirect("/");
        }

        $logs = $this->Log->find("all",
            array(
                "conditions" => array(
                    "package_name" => $id
                ),
                "order" => "created DESC"
            )
        );

        if(count($logs) == 0) {
            $this->redirect("/");
        }

        $this->set("logs", Set::extract("/Log/.", $logs));
        $this->set("title_for_layout", "Bug tracker 2.0");

    }

    public function deleteLog($id) {
        if(empty($id)) {
            $this->redirect("/");
        }

        if($this->Log->delete($id)) {
            $this->_setFlash("Row log deleted", "success");
        } else {
            $this->_setFlash("Row log deleting error", "error");
        }

        $this->redirect($this->referer());

    }

    public function deleteAppLogs($id) {
        if(empty($id)) {
            $this->redirect("/");
        }

        if ($this->Log->deleteAll(array("package_name" => $id))) {
            $this->_setFlash("Logs for " . $id . " removed", "success");
        } else {
            $this->_setFlash("Log deleting error", "error");
        }

        $this->redirect($this->referer());
    }
}
